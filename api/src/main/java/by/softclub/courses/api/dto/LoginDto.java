package by.softclub.courses.api.dto;

import lombok.Getter;
import lombok.Setter;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class LoginDto {
    @NotNull
    @NotEmpty(message="login is empty")
    private String login;

    @NotNull
    @NotEmpty(message="password is empty")
    private String password;
}
