package by.softclub.courses.api.service;

import by.softclub.courses.api.dto.OrderDto;

public interface KafkaService {

    void send(OrderDto dto);
}
