package by.softclub.courses.api.service.impl;

import by.softclub.courses.api.dto.OrderDto;
import by.softclub.courses.api.service.KafkaService;
import by.softclub.courses.api.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    KafkaService kafkaService;

    @Override
    public void send(OrderDto orderDto) {
        kafkaService.send(orderDto);
    }
}
