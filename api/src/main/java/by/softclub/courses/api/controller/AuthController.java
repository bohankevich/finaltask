package by.softclub.courses.api.controller;

import by.softclub.courses.api.domain.User;
import by.softclub.courses.api.dto.LoginDto;
import by.softclub.courses.api.service.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class AuthController {

    @Autowired
    AuthenticationService authenticationService;

    @PostMapping(value="/login")
    public String login(@Valid @RequestBody LoginDto dto){
        User user = (User) authenticationService.loadUserByUsername(dto.getLogin());
        return authenticationService.login(user, dto);
    }
}
