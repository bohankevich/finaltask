package by.softclub.courses.api.service.impl;

import by.softclub.courses.api.config.jwt.JwtTokenProvider;
import by.softclub.courses.api.domain.User;
import by.softclub.courses.api.dto.LoginDto;
import by.softclub.courses.api.repository.UserRepository;
import by.softclub.courses.api.service.AuthenticationService;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.map.IMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
@CacheConfig(cacheNames = "users")
@Transactional
public class AuthenticationServiceImpl implements AuthenticationService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    @Qualifier("hazelcastInstance")
    private HazelcastInstance hazelcastInstance;

    private static final String CACHE_NAME = "users";

    @Override
    public void replaceUserInCache(String token, User user) {
        IMap<Object, Object> map = hazelcastInstance.getMap(CACHE_NAME);
        user.setPreviousToken(token);
        map.put(user.getUsername(), user, 20, TimeUnit.MINUTES);
    }

    @Override
    public String login(User user, LoginDto loginDto) {
        if (!passwordEncoder.matches(loginDto.getPassword(), user.getPassword())) {
            throw new RuntimeException();
        }

        String token = jwtTokenProvider.createToken(user.getUsername(), user.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList()));

        var now = LocalDateTime.now();
        user.setLoginDate(now);
        user.setPreviousToken(token);
        replaceUserInCache(token, user);

        return token;
    }

    @Cacheable
    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return userRepository.findByUserNameEquals(s);
    }
}
