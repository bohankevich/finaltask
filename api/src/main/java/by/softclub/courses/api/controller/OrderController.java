package by.softclub.courses.api.controller;

import by.softclub.courses.api.dto.OrderDto;
import by.softclub.courses.api.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/order")
public class OrderController {

    @Autowired
    OrderService orderService;

    @PostMapping(value = "/add")
    public void addOrder(@Valid @RequestBody OrderDto dto) {
        orderService.send(dto);
    }
}
