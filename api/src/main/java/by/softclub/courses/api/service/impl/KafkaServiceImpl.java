package by.softclub.courses.api.service.impl;

import by.softclub.courses.api.dto.OrderDto;
import by.softclub.courses.api.service.KafkaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaServiceImpl implements KafkaService {

    @Autowired
    private KafkaTemplate<Long, OrderDto> kafkaTemplate;

    @Override
    public void send(OrderDto dto) {
        kafkaTemplate.send("orders", dto);
    }
}
