package by.softclub.courses.api.service;

import by.softclub.courses.api.domain.User;
import by.softclub.courses.api.dto.LoginDto;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface AuthenticationService extends UserDetailsService {
    void replaceUserInCache(String token, User user);

    String login(User user, LoginDto loginDto);
}
