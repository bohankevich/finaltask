package by.softclub.courses.api.repository;

import by.softclub.courses.api.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {
    User findByUserNameEquals(String userName);
}
