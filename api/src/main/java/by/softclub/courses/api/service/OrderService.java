package by.softclub.courses.api.service;

import by.softclub.courses.api.dto.OrderDto;

public interface OrderService {
    void send(OrderDto orderDto);
}
