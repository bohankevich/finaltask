package by.softclub.courses.api;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootTest
class ApiApplicationTests {

    @Test
    void contextLoads() {
    }

    @Autowired
    PasswordEncoder passwordEncoder;

    @Test
    void generatePassword() throws Exception {
        String res = passwordEncoder.encode("admin");

        System.out.println(res);

        Assertions.assertTrue(passwordEncoder.matches("admin", res));
    }
}
