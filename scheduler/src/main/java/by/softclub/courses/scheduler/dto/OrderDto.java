package by.softclub.courses.scheduler.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class OrderDto {

    private String userName;

    private String fromCurrency;

    private String toCurrency;

    private BigDecimal amount;
}
