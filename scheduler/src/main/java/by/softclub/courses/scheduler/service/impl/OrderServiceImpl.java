package by.softclub.courses.scheduler.service.impl;

import by.softclub.courses.scheduler.domain.Order;
import by.softclub.courses.scheduler.dto.OrderDto;
import by.softclub.courses.scheduler.repository.CurrencyRepository;
import by.softclub.courses.scheduler.repository.OrderRepository;
import by.softclub.courses.scheduler.repository.UserRepository;
import by.softclub.courses.scheduler.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@CacheConfig(cacheNames = "orders")
@Transactional
public class OrderServiceImpl implements OrderService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    CurrencyRepository currencyRepository;

    @Autowired
    OrderRepository orderRepository;

    @Override
    public void save(OrderDto dto) {
        var order = new Order();

        order.setUser(userRepository.findByUserNameEquals(dto.getUserName()));
        order.setAmount(dto.getAmount());

        currencyRepository.findByCode(dto.getFromCurrency()).ifPresent(
                order::setFromCurrency
        );
        currencyRepository.findByCode(dto.getToCurrency()).ifPresent(
                order::setToCurrency
        );

        orderRepository.save(order);
    }
}
