package by.softclub.courses.scheduler.repository;

import by.softclub.courses.scheduler.domain.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, Integer> {

}
