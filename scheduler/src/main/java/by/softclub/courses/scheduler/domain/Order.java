package by.softclub.courses.scheduler.domain;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Getter
@Setter
@Table(name="SC_ORDER")
public class Order {

    @Id
    @GeneratedValue(generator = "SEQ_ORDER", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "SEQ_ORDER", sequenceName = "SEQ_ORDER", allocationSize = 1)
    private int id;

    @ManyToOne
    private Currency fromCurrency;

    @ManyToOne
    private Currency toCurrency;

    @NotNull
    private BigDecimal amount;

    @ManyToOne
    private User user;
}
