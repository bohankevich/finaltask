package by.softclub.courses.scheduler.service;

import by.softclub.courses.scheduler.dto.OrderDto;

public interface OrderService {
    void save(OrderDto dto);
}
