package by.softclub.courses.scheduler.service.impl;

import by.softclub.courses.scheduler.dto.OrderDto;
import by.softclub.courses.scheduler.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class KafkaMessageServiceImpl {

    @Autowired
    OrderService orderService;

    @KafkaListener(id = "${kafka.producer.id}", topics = {"orders"}, containerFactory = "singleFactory")
    public void consume(OrderDto dto) {
        orderService.save(dto);
    }
}
