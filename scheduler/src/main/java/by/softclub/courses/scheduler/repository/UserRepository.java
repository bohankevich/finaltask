package by.softclub.courses.scheduler.repository;

import by.softclub.courses.scheduler.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {
    User findByUserNameEquals(String userName);
}
